'use strict'

function changeFormat (item) { // изменение формата вывода для пользователя
    
    switch(item) {        
        case 0: item = '00';break;
        case 1: item = '01';break;
        case 2: item = '02';break;
        case 3: item = '03';break;
        case 4: item = '04';break;
        case 5: item = '05';break;
        case 6: item = '06';break;
        case 7: item = '07';break;
        case 8: item = '08';break;
        case 9: item = '09';break;
    }

    return item;
}

setInterval (function() {
    
    var now = new Date(),                               // создаем объект Дата
        month = now.getMonth(),
        date = now.getDate(),
        day = now.getDay(),
        hour = now.getHours(),                          // получаем часы
        minutes = now.getMinutes(),                     // получаем минуты
        seconds = now.getSeconds();                     // получаем секунды

    switch (month) {
        case 0: month = 'января'; break;
        case 1: month = 'февраля'; break;
        case 2: month = 'марта'; break;
        case 3: month = 'апреля'; break;
        case 4: month = 'мая'; break;
        case 5: month = 'июня'; break;
        case 6: month = 'июля'; break;
        case 7: month = 'августа'; break;
        case 8: month = 'сентября'; break;
        case 9: month = 'октября'; break;
        case 10: month = 'ноября'; break;
        case 11: month = 'декабря'; break;
    }

    switch (day) {
        case 0: day = 'вс'; break;
        case 1: day = 'пн'; break;
        case 2: day = 'вт'; break;
        case 3: day = 'ср'; break;
        case 4: day = 'чт'; break;
        case 5: day = 'пт'; break;
        case 6: day = 'сб'; break;
    }

var dateShow = document.querySelector('.b-date');           // находим блок для вставки времени

dateShow.innerHTML = (date + ' ' + month + ', ' + day + '<br><span id="b-date_color">' + hour + ':' + changeFormat(minutes)  + ':' + changeFormat(seconds) + '</span>'); // вставляем время
if (hour >= 17 || hour < 8 || hour == 12 ) {
    var dateColor = document.getElementById('b-date_color');
    //console.log(dateColor);
    dateColor.style.color = 'red';
}


}, 1000);                                                                             // обновляем время каждую секунду

