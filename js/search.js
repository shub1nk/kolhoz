"use strict"

// 1. Живой поиск
// 2. Запрет английских символов и первого пробела

function banSymbol(search) {

    // получить поисковую строку и разбить ее в массив
    var banMessage = document.getElementById('ban-message');
    var banSymbol = search.split('');
    //найти в ней английские символы
    var banSymbolTemplate = /[A-Za-z_<>,.@:;"'=\/\[\]]/g; // шаблон запрещенных символов    
    // получить позицию английского символа
    for (var i = 0; i < banSymbol.length; /*шаг итерации задается только если символ не из запрещенных*/) {                  // обход всех символов        

        if (banSymbol[i].search(banSymbolTemplate) !== -1 || banSymbol[0].search(/\s/) === 0) { //если есть совпадения            
            banSymbol.splice(i, 1);            // удалить из массива этот символ
            banMessage.style.display = 'block';
            setTimeout(
                function () {
                    banMessage.style.display = 'none';
                }
                , 5000
            )
            /*if (banSymbol[0] === /\s/) {
                banSymbol.splice(0, 1);                        // удалить из массива этот символ
                console.log('удаляем')
            }*/
        } else {
            i++;  // в противном случае, увеличиваем индекс на 1 и проверяем следующий символ
            banMessage.style.display = 'none';
        }
    }

    document.getElementById('searchValue').value = banSymbol.join(''); // меняем значение в поисковой строке
    search = banSymbol.join(''); // склеиваем все символы в поисковый запрос, для передачи в функцию поиска
    
    return search;

    // ----------------------------------------------------------------------------------
}

// 3. Блок с сообщением об отсутствии результатов поиска
// 4. Удаление результатов поиска
// 5. Вывод количества совпадений

$(document).ready(function () {

    var departments = $('.b-department'); //собираем все отделы    

    var $searchValue = $('#searchValue'); //зацепляемся за строку поиска

    $searchValue.on('input', function () {

        var $departments = departments.clone(); //делаем копию массива с отделами

        var search = $searchValue.val();

        if (banSymbol(search)) {
            $('.b-office').hide();  //скрываем оригинальный блок контактов 
            $('.all-full').hide();
            $('.all-null').hide();
            $('#searchBack').show();
            $('#concurrences').show();
            $('.b-department').not(':hidden').remove();
            $('.b-contacts').append($departments);
            $departments.addClass('height-full');
            var searchRequest = new RegExp('(' + search + ')', 'gi');

            $departments.each(function () {
                var $employees = $(this).children('.b-employees');

                $employees.each(function () {
                    var $tr = $(this).find('tr');
                    $tr.each(function () {
                        var $text = $(this).text();
                        if (!searchRequest.test($text)) {
                            $(this).remove();
                        } else {
                            //$(this).show();
                            $(this).html($(this).html().replace(searchRequest, '<mark>$1</mark>'));
                        }
                    })
                });
            })

            $departments.each(function () {
                if ($(this).find('tr').length === 0) {
                    $(this).remove();
                }
            });
            $('#concurrences-num').text($('.b-department tr').not(':hidden').length);
            $('.b-wrapper').css('overflow-y', 'auto');
        } else {
            $('.all-full').show();
            $('.all-null').show();
            $('.b-office').show();
            $('#searchBack').hide();
            $('#concurrences').hide();
            //$('.b-department').not(':hidden').remove();
            //$('.b-office').show();  //показываем оригинальный блок контактов    
        }

        //$departments.on('click', function () {
        //    $(this).toggleClass('height-full')
        //});

    });

    $('#searchBack').click(function () { //Кнопка очистки строки поиска
        $('#searchValue').val('');
        $('.all-full').show();
        $('.all-null').show();
        $('.b-office').show();
        $('#searchBack').hide();
        $('#concurrences').hide();
    })

});



//
//// скрываем все переданные элементы
//function hideItems(item) {
//    for (var i = 0; i < item.length; i++) {
//        item[i].style.display = 'none';
//    }
//}
//
//// показать все переданные элементы
//function showItems(item) {
//    for (var i = 0; i < item.length; i++) {
//        item[i].style.display = 'block';
//    }
//}
//
//
//
//// начинаем поиск
//function searchContacts(search) {
//
//    hideItems(contactsOriginal);
//    hideItems(buttonAll);
//
//    var searchRequest = new RegExp('(' + search + ')', 'ig') // помещаем поисковый запрос в регулярное выражение
//    var contacts = []; // создаем массив, в который будем помещать наши строки
//
//    for (var i = 0; i < listContact.length; i++) { // запускаем цикл обхода всех строк
//
//        var trHtml = listContact[i].innerHTML; // присваиваем переменную html-строки
//
//        if ((trHtml.search(searchRequest)) > 0) {
//            contacts.push(listContact[i]);
//        }
//    }
//
//    // создание таблицы для вывода результатов
//    var tableSearchResult = document.createElement('table');
//    tableSearchResult.classList.add('search-result');
//    wrapper.appendChild(tableSearchResult);
//
//    // создание блока с сообщением о безуспешном поиске
//    var blokSearchNull = document.createElement('div');
//    blokSearchNull.classList.add('blokSearchNull');
//    blokSearchNull.innerHTML = 'Совпадений не найдено! Попробуйте ввести другое значение или очистить строку поиска, нажав кнопку <img src="css/img/searchBack.png" width="16" height="16">'
//    //-------------------------------------------------
//
//    for (var j = 0; j < contacts.length; j++) {
//        var trSearchResult = document.createElement('tr');
//
//        var spanAllotment = contacts[j].innerHTML;
//
//        trSearchResult.innerHTML = spanAllotment.replace(searchRequest, '<mark>$1</mark>')
//
//        tableSearchResult.appendChild(trSearchResult);
//    }
//
//    var concurrencesNum = document.getElementById('concurrences-num');
//    concurrencesNum.innerText = contacts.length;
//
//    var blokSearchNullDelete = document.querySelector('div.blokSearchNull');
//
//    if (blokSearchNullDelete) {
//        blokSearchNullDelete.parentNode.removeChild(blokSearchNullDelete);
//    }
//    //
//    if (contacts.length == 0) {
//        wrapper.appendChild(blokSearchNull);
//        tableSearchResult.parentNode.removeChild(tableSearchResult)
//    }
//
//    // убираем полосу прокрутки, если результат поискового запроса по высоте меньше родительского блока
//    if (tableSearchResult.offsetHeight < wrapper.offsetHeight) {
//        //console.log(tableSearchResult.offsetHeight + '<' + wrapper.offsetHeight)
//        wrapper.style.overflowY = 'auto';        
//    }
//}
//
//function contactReturn() {    // функция возврата блока контактов и кнопок "Свернуть/Развернуть"
//
//    showItems(contactsOriginal);
//    showItems(buttonAll);
//}
//
//function deleteBackspace(event, count) {    // при нажатии delete и backspace вызывается функция поиска
//    switch (event.keyCode) {
//        case 8: searchContacts(search); break;
//        case 46: searchContacts(search); break;
//    }
//}
//
/////////////////////////////////////////////////////////////////////////////
//
//var wrapper = document.querySelector('.b-wrapper'), // блок обертки - в него будем вставлять результат поиска
//    contactsOriginal = document.querySelectorAll('.b-contacts'), // переменная содержит в себе блок с контактами
//    searchValue = document.getElementById('searchValue'), // переменная содержит в себе строку поиска    
//    office = document.querySelectorAll('.b-office_header'), // массив с офисами
//    //department = document.querySelectorAll('.b-department_header'), // массив с отделами
//    buttonAll = document.querySelectorAll('.b-button-all'), // кнопка присвоена массиву, чтобы срабатывала функция для скрывания элементов
//    listContact = document.querySelectorAll('tr'); // массив со всеми контактами
//
//
//searchValue.oninput = function () {
//
//    var search = document.getElementById('searchValue').value, // значение из строки поиска
//        concurrences = document.getElementById('concurrences'), // количество совпадений   
//        searchBack = document.getElementById('searchBack'); // кнопка очистки поисковой строки
//
//    banSymbol(search);
//
//    var removeTableSearchResult = document.querySelector('table.search-result');    
//
//    if (!banSymbol(search)) {
//        setTimeout(function () {
//            searchBack.style.display = 'none';
//            concurrences.style.display = 'none';
//        }, 300);
//        contactReturn(); // переписать функцию под новые требования         
//    } else {
//        searchBack.style.display = 'block';
//        concurrences.style.display = 'block';
//        searchContacts(search);
//    }
//
//    if (removeTableSearchResult) {        
//        removeTableSearchResult.parentNode.removeChild(removeTableSearchResult);        
//    }
//}
//
//searchBack.onclick = function () {  // очистка строки поиска и возврат контактов
//
//    document.getElementById('searchValue').value = '';
//    var removeTableSearchResult = document.querySelector('table.search-result');    
//
//    if (removeTableSearchResult) {
//        removeTableSearchResult.parentNode.removeChild(removeTableSearchResult);
//    }
//
//    contactReturn();
//
//    setTimeout(
//        function () {
//            searchBack.style.display = 'none';
//            concurrences.style.display = 'none';
//        }, 300);    
//}
//
//