"use strict"

$(document).ready(function () {

    // Разворачиваем/сворачиваем 1 отдел

    var $departments = $('div.b-department');

    $departments.on('click', '.b-department_header', function () {
        $(this).parent().toggleClass('height-full');
    });

    // Разворачиваем/сворачиваем все отделы

    $('.all-full').on('click', function () {
        $departments.addClass('height-full');
    });

    $('.all-null').on('click', function () {
        $departments.removeClass('height-full');
    });

});